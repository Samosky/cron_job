<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class WordOfTheDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'word:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily email to every users with a word and every meaning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        

        // Finding a random word
        $message = "Today is a good day to go to the immigration office to get your passport";
        Mail::raw("{$message}", function($mail){
            $email ='samsonadejoro@gmail.com';
            $mail->from('samsonadejoro@gmail.com');
            $mail->to($email)
                ->subject('A reminder! You need to get your PASSPORT!');
        });
        
        $this->info("Today's message has been successfully sent.");
    }
}
