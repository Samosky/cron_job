<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('foo', function () {

    $message = "Today is a good day to go to the immigration office to get your passport";
    Mail::raw("{$message}", function($mail){
        $email ='samsonadejoro@gmail.com';
        $mail->from('samsonadejoro@gmail.com');
        $mail->to($email)
            ->subject('A reminder! You need to get your PASSPORT!');
    });
    return "sucess";
});
